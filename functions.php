
<?php


class Dbc_Single_Product_Site{
	public function __construct(){
		add_action('woocommerce_cart_product_cannot_add_another_message', [$this, "redirect_if_already_in_cart"],10,2);
		add_filter('add_to_cart_redirect', [$this, 'add_to_cart_redirect']);
		add_filter( 'woocommerce_enable_order_notes_field', '__return_false' );
		add_filter( 'wc_add_to_cart_message_html', '__return_false' );

	}

	public function redirect_if_already_in_cart($message, $product_data){
		// return "This product already exists in your cart";
		global $woocommerce;
		$redirect_checkout = $woocommerce->cart->get_checkout_url();
		wp_redirect($redirect_checkout);
		die();
	}

	function add_to_cart_redirect() {
		global $woocommerce;
		$redirect_checkout = $woocommerce->cart->get_checkout_url();
		return $redirect_checkout;
	}
}
$Dbc_Single_Product_Site = new Dbc_Single_Product_Site();

